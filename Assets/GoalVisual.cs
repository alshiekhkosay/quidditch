using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class GoalVisual : MonoBehaviour
{

    [SerializeField] private ChaserPlayer chaserPlayer;
    [SerializeField] private Transform goalVisual;

    private void Awake()
    {
        goalVisual.gameObject.SetActive(false);
    }

    private void Update()
    {
       
        if (chaserPlayer.selectedGoal == transform)
        {
            goalVisual.gameObject.SetActive(true);
        }
        else
        {
            goalVisual.gameObject.SetActive(false);
        }
    }

}
