using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BludgerAnimator : MonoBehaviour
{
    private const string IS_DESTROYED = "IsDestroyed";
    [SerializeField] private BludgerBall bludgerBall;
    [SerializeField] private Material material;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    private void Start()
    {
        bludgerBall.OnBludgerDestroyStatusChange += bludgerBall_OnBludgerDestroyStatusChange;
    }

    private void bludgerBall_OnBludgerDestroyStatusChange(object sender, EventArgs e)
    {
        animator.SetBool(IS_DESTROYED, bludgerBall.IsDestroyed);
        if (bludgerBall.IsDestroyed)
        {
            material.EnableKeyword("_EMISSION");
        }
        else
        {
            material.DisableKeyword("_EMISSION");

        }

    }
}
