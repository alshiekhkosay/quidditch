using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    private Vector2 mouseRotation;
    private float leftRightBallAimingSensitivty = 2;
    private float UpDownBallAimingSensitivty = 4;

    private void LateUpdate()
    {

        if (Input.GetMouseButton(1))
        {
            mouseRotation.x += (Input.GetAxis("Mouse X") * UpDownBallAimingSensitivty);
            mouseRotation.y += (Input.GetAxis("Mouse Y") * leftRightBallAimingSensitivty);

            transform.localRotation = Quaternion.Euler(-mouseRotation.y, mouseRotation.x, 0);
        }
        if (Input.GetMouseButtonUp(1))
        {
            mouseRotation = Vector2.zero;
        }

    }
}
