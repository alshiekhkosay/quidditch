using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TeamMateChaserDetecter : MonoBehaviour
{
    [SerializeField] private ChaserPlayer chaserPlayer;

    private Transform detectedTarget;
    private void OnTriggerStay(Collider other)
    {
        foreach (var player in chaserPlayer.playersICanPassTo)
        {

            if (player == other.transform)
            {

                detectedTarget = other.transform;
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (var player in chaserPlayer.playersICanPassTo)
        {

            if (player == other.transform)
            {
                detectedTarget = null;
            }

        }
    }
    public Transform GetDetectedTarget()
    {
        return detectedTarget;
    }
}
