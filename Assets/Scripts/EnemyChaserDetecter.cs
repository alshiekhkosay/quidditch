using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaserDetecter : MonoBehaviour
{
    [SerializeField] private ChaserPlayer chaserPlayer;

    private Transform detectedTarget;
    private void OnTriggerStay(Collider other)
    {
        foreach (var player in chaserPlayer.enemyChaseres)
        {
           
            ChaserPlayer enemyChaser = player.GetComponent<ChaserPlayer>();
            if (other.TryGetComponent<EnemyChaserDetecter>(out EnemyChaserDetecter enemyChaserDetecter))
            {
                 Debug.Log("You can Steel");
                if (enemyChaserDetecter == enemyChaser.enemyChaserDetecter)
                {
                    detectedTarget = player;
                }
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<EnemyChaserDetecter>(out _))
        {

            detectedTarget = null;
        }
    }
    public Transform GetDetectedTarget()
    {
        return detectedTarget;
    }
}
