using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Unity.VisualScripting;


public class ChaserPlayer : Player
{
    private bool iCanPass = false;
    private bool iCanShoot = false;
    private bool iCanSteal = false;
    private bool iCanCatch = false;
    private bool iAmThrowingBall = false;
    private float passingDistance = 8f;
    private float shootingDistance = 30f;
    private float stealingRadius = 0.1f;
    private float stealingDistance = 0.4f;
    private float catchingRaduis = 0.5f;
    private float catchingDistance = 0.8f;

    static public event EventHandler<OnChaserPassArguments> OnChaserPass;
    public class OnChaserPassArguments : EventArgs
    {
        public float throwSpeed;
        public Transform targetChaser;
        public float throwAngle;
    }
    static public event EventHandler<OnChaserShootArguments> OnChaserShoot;
    public class OnChaserShootArguments : EventArgs
    {
        public float throwSpeed;
        public Transform targetGoal;
        public float throwAngle;
    }
    static public event EventHandler<OnChaserStealArguments> OnChaserSteal;
    public class OnChaserStealArguments : EventArgs
    {
        public Transform myBallHoldingPosition;
        public Transform targetChaser;
    }
    static public event EventHandler OnChaserCatch;
    // public class OnChaserCatchArguments : EventArgs
    // {
    //     public Transform myBallHoldingPosition;
    // }


    [SerializeField] private Transform catchingSphereCastStartingPoint;
    [SerializeField] private TeamMateChaserDetecter teamMateChaserDetecter;
    [SerializeField] public EnemyChaserDetecter enemyChaserDetecter;

    [SerializeField]
    private Button catchingButton;
    [SerializeField]
    public Transform[] playersICanPassTo;
    [SerializeField]
    private Transform[] enemyGoalPosts;
    [SerializeField]
    public Transform[] enemyChaseres;
    [SerializeField] private LayerMask goalsLayerMask;

    private bool isStunned;

    private float stunningTimer;
    private float stunningTimerMax = 3f;

    private float throwAngle = 30f;

    public Transform selectedGoal;

    private void Start()
    {
        flySpeed = defaultFlySpeed;
        BludgerBall.OnBludgerHit += BludgerBall_OnBludgerHit;
        OnChaserCatch += Chaser_OnOtherChaserCatch;
        OnChaserPass += Chaser_OnOtherChaserPass;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }



    private void Chaser_OnOtherChaserPass(object sender, OnChaserPassArguments e)
    {
        if (e.targetChaser.GetComponent<ChaserPlayer>() == this)
        {
            flySpeed = 0;
        }
    }

    private void Chaser_OnOtherChaserCatch(object sender, EventArgs e)
    {
        if ((ChaserPlayer)sender != this)
        {
            iAmHoldingBall = false;
            iAmThrowingBall = false;
        }
        flySpeed = defaultFlySpeed;
    }

    private void BludgerBall_OnBludgerHit(object sender, BludgerBall.OnBludgerHitArgs e)
    {
        if (e.hittedPlayerCollider.GetComponent<ChaserPlayer>() == this)
        {
            isStunned = true;
            stunningTimer = stunningTimerMax;
        }
    }

    private void Update()
    {
        Debug.DrawLine(ballHoldingPosition.position, ballHoldingPosition.position + transform.forward * shootingDistance);
        Movement();
        BallAiming();
        CastDetection();
        StunPlayer();

    }



    private void StunPlayer()
    {
        if (isStunned)
        {
            flySpeed = 0f;
            stunningTimer -= Time.deltaTime;
            if (stunningTimer < 0)
            {
                flySpeed = defaultFlySpeed;
                isStunned = false;
            }
        }
    }

    private void BoxDetectionForPassingQuaffleBall()
    {
        Transform detectedTarget = teamMateChaserDetecter.GetDetectedTarget();
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log(detectedTarget);
            if (detectedTarget != null && iAmHoldingBall)
            {
                float throwSpeed = calculateThrowSpeed(detectedTarget.GetComponent<ChaserPlayer>().catchingSphereCastStartingPoint.position);
                Debug.Log(throwSpeed);
                if (!float.IsNaN(throwSpeed))
                {

                    iAmThrowingBall = true;
                    iAmHoldingBall = false;
                    OnChaserPass?.Invoke(this, new OnChaserPassArguments
                    {
                        throwSpeed = throwSpeed,
                        targetChaser = detectedTarget,
                        throwAngle = throwAngle,

                    });

                }
            }
        }
    }



    private void SphereDetectionForStealingQuaffleBall()
    {
        Transform enemyChaser = enemyChaserDetecter.GetDetectedTarget();
        if (enemyChaser != null && !iAmHoldingBall && enemyChaser.GetComponent<ChaserPlayer>().iAmHoldingBall)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {


                OnChaserSteal?.Invoke(this, new OnChaserStealArguments
                {
                    myBallHoldingPosition = ballHoldingPosition,
                    targetChaser = enemyChaser
                });
            }
        }
        // if (Physics.SphereCast(ballHoldingPosition.position, 1f, transform.forward, out RaycastHit stealTarget, 0.5f))
        // {
        //     foreach (Transform enemyChaser in enemyChaseres)
        //     {
        //         hitPosition = stealTarget.distance;
        //         Debug.Log("You Can Steal");
        //         if (enemyChaser == stealTarget.transform)
        //         {
        //             iCanSteal = true;
        //             if (Input.GetKeyDown(KeyCode.C) && iCanSteal == true)
        //             {
        //                 OnChaserSteal?.Invoke(this, new OnChaserStealArguments
        //                 {
        //                     myBallHoldingPosition = ballHoldingPosition,
        //                     targetEnemyChaserBallHoldingPosition = stealTarget.transform.GetComponent<ChaserPlayer>().ballHoldingPosition

        //                 });

        //             }
        //         }
        //     }
        // }
    }
    private void RayCastDetectionForShootingQuaffleBall()
    {
        if (Physics.SphereCast(transform.position, 4f, transform.forward, out RaycastHit shootTarget, shootingDistance, goalsLayerMask))
        {
            selectedGoal = shootTarget.transform;

            foreach (Transform goalPost in enemyGoalPosts)
            {

                iCanShoot = goalPost == shootTarget.transform && iAmHoldingBall;
                Debug.Log(iCanShoot);
                if (iCanShoot)
                {

                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        float throwSpeed = calculateThrowSpeed(shootTarget.point);
                        if (!float.IsNaN(throwSpeed))

                            OnChaserShoot?.Invoke(this, new OnChaserShootArguments
                            {
                                throwSpeed = throwSpeed,
                                targetGoal = shootTarget.transform,
                                throwAngle = throwAngle
                            });
                    }
                }

            }

        }
        else
        {
            selectedGoal = null;
        }

    }

    private float calculateThrowSpeed(Vector3 deliveryPosition)
    {

        Vector2 deliveryPoint = new Vector2(deliveryPosition.z - QuaffleBall.instance.transform.position.z, deliveryPosition.y - QuaffleBall.instance.transform.position.y);
        float distance = Vector2.Distance(new Vector2(deliveryPosition.x, deliveryPosition.z), new Vector2(QuaffleBall.instance.transform.position.x, QuaffleBall.instance.transform.position.z));
        float SpeedValue = Mathf.Sqrt(9.81f * Mathf.Pow(distance, 2) / (2 * Mathf.Pow(Mathf.Cos(throwAngle * Mathf.Deg2Rad), 2) * (Mathf.Abs(distance) * Mathf.Tan(throwAngle * Mathf.Deg2Rad) - deliveryPoint.y)));
        return SpeedValue;
    }


    private void SphereCastDetectionForCatchingQuaffleBall()
    {
        if (!iAmHoldingBall)
        {
            if (Physics.SphereCast(catchingSphereCastStartingPoint.position, catchingRaduis, transform.forward, out RaycastHit quaffleBall, catchingDistance))
            {

                if (quaffleBall.transform.TryGetComponent<QuaffleBall>(out QuaffleBall quaffle))
                {

                    if (iAmThrowingBall == false && quaffle.MyChaser == null)
                    {

                        OnChaserCatch?.Invoke(this, EventArgs.Empty);
                        iAmHoldingBall = true;
                        flySpeed = defaultFlySpeed;
                    }

                }
            }
        }
    }
    private void CastDetection()
    {
        BoxDetectionForPassingQuaffleBall();
        SphereDetectionForStealingQuaffleBall();
        RayCastDetectionForShootingQuaffleBall();
        SphereCastDetectionForCatchingQuaffleBall();
    }

}
