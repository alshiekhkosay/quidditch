using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool iAmHoldingBall;
    protected bool iAmStuned;
    [SerializeField]
    public Transform ballHoldingPosition;
    [SerializeField] protected float defaultFlySpeed = 10f;
    protected float flySpeed;
    [SerializeField]
    private float MovementSpeed = 50f;
    [SerializeField]
    private float aimSpeed;
    [SerializeField]
    private Transform myCamera;
    float leftRightMovement;
    float upDownMovement;
    private bool isPlayerMovingForward;
    private Vector2 mouseRotation;
    private float leftRightBallAimingSensitivty = 2;
    private float UpDownBallAimingSensitivty = 4;


    private void Start()
    {
       
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        Movement();
        // BallAiming();
    }
    public bool isMovingForward()
    {
        return isPlayerMovingForward;
    }

    protected void Movement()
    {
        leftRightMovement = Input.GetAxisRaw("Horizontal");
        upDownMovement = Input.GetAxisRaw("Vertical");

        transform.Rotate(Vector3.up * leftRightMovement * 50f * Time.deltaTime, Space.Self);
        transform.Rotate(Vector3.right * upDownMovement * 50f * Time.deltaTime, Space.Self);

        if (Input.GetKeyDown("q"))
        {
            transform.Rotate(new Vector3(0, 180, 0));
        }

        if (Input.GetKey("space"))
        {
            Debug.Log("moving forward");
            transform.Translate(Vector3.forward * flySpeed * Time.deltaTime);
            isPlayerMovingForward = true;
        }
        else
        {
            isPlayerMovingForward = false;
        }
    }
    protected void BallAiming()
    {
        if (Input.GetMouseButton(1))
        {
            mouseRotation.x += (Input.GetAxis("Mouse X") * UpDownBallAimingSensitivty);
            mouseRotation.y += (Input.GetAxis("Mouse Y") * leftRightBallAimingSensitivty);
            ballHoldingPosition.localRotation = Quaternion.Euler(-mouseRotation.y, mouseRotation.x, 0);
        }
    }




}
