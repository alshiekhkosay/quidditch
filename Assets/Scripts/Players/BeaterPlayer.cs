using System;
using Cinemachine;
using Unity.VisualScripting;
using UnityEngine;

public class BeaterPlayer : Player
{
    public event EventHandler<OnBeaterThrowArgs> OnBeaterThrow;

    static public event EventHandler<OnBeaterDestroyArgs> OnBeaterDestroy;
    public class OnBeaterThrowArgs : EventArgs
    {
        public Vector3 position;
    }

    public class OnBeaterDestroyArgs : EventArgs
    {
        public BludgerBall bludgerBallToDistroy;
    }

    public bool InAimMode { get; private set; }
    public bool IsHoldingBludgerBall { get; set; }


    [SerializeField] private BludgerBall bludgerBall;
    [SerializeField] private Transform firstPersonCam;



    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();



    private void Awake()
    {
        //For Developing purposes, must be removed 
        if (bludgerBall != null)
        {
            IsHoldingBludgerBall = true;

        }
    }

    private void Start()
    {
        flySpeed = defaultFlySpeed;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {

        TuggleAimMode();
        GetDirectionAndThrow();
        Movement();

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<BludgerBall>(out BludgerBall bludgerBall))
        {
            if (Input.GetKey(KeyCode.F))
            {
                OnBeaterDestroy?.Invoke(this, new OnBeaterDestroyArgs
                {
                    bludgerBallToDistroy = bludgerBall
                });

            }
        }
    }



    private void GetDirectionAndThrow()
    {
        if (InAimMode)
        {
            Vector2 ScreenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
            Ray ray = Camera.main.ScreenPointToRay(ScreenCenterPoint);

            Debug.Log("In Aim Mode");
            if (Physics.Raycast(ray, out RaycastHit hitInfo, 999f, aimColliderLayerMask))
            {
                Debug.Log("You can Throw");

                if (Input.GetMouseButtonDown(0))
                {

                    IsHoldingBludgerBall = false;
                    OnBeaterThrow?.Invoke(this, new OnBeaterThrowArgs
                    {
                        position = hitInfo.point
                    });
                }
            }
        }
    }

    private void TuggleAimMode()
    {
        //For Developing purposes, must be removed 
        if (bludgerBall == null) return;


        if (!bludgerBall.IsDestroyed && IsHoldingBludgerBall)
        {
            if (Input.GetMouseButton(1))
            {
                InAimMode = true;

                firstPersonCam.GetComponent<CinemachineFreeLook>().Priority = 2;
            }
            if (Input.GetMouseButtonUp(1))
            {
                InAimMode = false;
                firstPersonCam.GetComponent<CinemachineFreeLook>().Priority = 0;

            }
        }
        else
        {
            InAimMode = false;
            firstPersonCam.GetComponent<CinemachineFreeLook>().Priority = 0;
        }

    }
}
