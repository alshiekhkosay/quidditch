using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeaterCrossHair : MonoBehaviour
{
    [SerializeField] private Image corssHairImage;

    [SerializeField] private BeaterPlayer beaterPlayer;

    private void Awake()
    {
        corssHairImage.gameObject.SetActive(false);
    }

    private void Update()
    {
        corssHairImage.gameObject.SetActive(beaterPlayer.InAimMode);
    }
}
