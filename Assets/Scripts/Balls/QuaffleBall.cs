using System;
using UnityEngine;
using UnityEngine.UI;
public class QuaffleBall : MonoBehaviour
{

    public static QuaffleBall instance { get; private set; }

    private Rigidbody rigidBody;

    public ChaserPlayer MyChaser { get; set; }



    private float gravity = 9.81f;

    private float ballStolenTimer;
    private float ballStolenTimerMax = 1f;

    private void Awake()
    {
        instance = this;
        rigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        ChaserPlayer.OnChaserPass += ChaserPlayer_OnChaserPass;
        ChaserPlayer.OnChaserCatch += ChaserPlayer_OnChaserCatch;
        ChaserPlayer.OnChaserSteal += ChaserPlayer_OnChaserSteal;
        ChaserPlayer.OnChaserShoot += ChaserPlayer_OnChaserShoot;
    }

    private void ChaserPlayer_OnChaserShoot(object sender, ChaserPlayer.OnChaserShootArguments e)
    {
        if ((ChaserPlayer)sender != MyChaser) return;
        Vector3 deliveryPosition = e.targetGoal.position;
        ThrowBall(deliveryPosition, e.throwSpeed, e.throwAngle);
    }

    private void ChaserPlayer_OnChaserSteal(object sender, ChaserPlayer.OnChaserStealArguments e)
    {

        if (e.targetChaser.TryGetComponent<ChaserPlayer>(out ChaserPlayer enemyChaser))
        {
            if (enemyChaser.iAmHoldingBall && ballStolenTimer < 0f)
            {
                ballStolenTimer = ballStolenTimerMax;
                MyChaser = (ChaserPlayer)sender;
                transform.position = MyChaser.ballHoldingPosition.position;
                Debug.Log(transform.position);
                transform.parent = MyChaser.ballHoldingPosition;
                transform.localPosition = Vector3.zero;
                MyChaser.iAmHoldingBall = true;
                enemyChaser.iAmHoldingBall = false;
            }

        }
    }

    private void ChaserPlayer_OnChaserCatch(object sender, EventArgs e)
    {
        Debug.Log("chaser catches");
        Debug.Log(highetPosition);

        MyChaser = (ChaserPlayer)sender;
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        transform.position = MyChaser.ballHoldingPosition.position;
        transform.parent = MyChaser.ballHoldingPosition;
        transform.localPosition = Vector3.zero;
        rigidBody.useGravity = false;
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }
    Vector3 highetPosition;
    void Update()
    {
        if (transform.position.y > highetPosition.y)
        {
            highetPosition = transform.position;
        }
        ballStolenTimer -= Time.deltaTime;
    }



    private void ChaserPlayer_OnChaserPass(object sender, ChaserPlayer.OnChaserPassArguments e)
    {
        Debug.Log("You throwed the ball");
        if ((ChaserPlayer)sender != MyChaser) return;
        Vector3 deliveryPosition = e.targetChaser.GetComponent<ChaserPlayer>().ballHoldingPosition.position;

        ThrowBall(deliveryPosition, e.throwSpeed, e.throwAngle);


    }

    private void ThrowBall(Vector3 deliveryPosition, float SpeedValue, float throwAngle)
    {
        Vector3 direction = deliveryPosition - transform.position;
        transform.parent = null;
        MyChaser = null;
        rigidBody.useGravity = true;
        direction.y = 0;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.localRotation = rotation;
        transform.localRotation *= Quaternion.Euler(-throwAngle, 0f, 0f);
        rigidBody.velocity = SpeedValue * transform.forward;
    }
}
