using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BludgerBall : MonoBehaviour
{


    public bool IsDestroyed { get; private set; }

    public event EventHandler OnBludgerDestroyStatusChange;
    static public event EventHandler<OnBludgerHitArgs> OnBludgerHit;

    public class OnBludgerHitArgs : EventArgs
    {
        public Collider hittedPlayerCollider;
    }

    [SerializeField] private BeaterPlayer beaterPlayer;

    [SerializeField] private Transform[] enemyChaseres;

    [SerializeField] private float throwPower = 2f;



    private new Rigidbody rigidbody;

    private float respawnAfterDestroyedTimer;

    private float respawnAfterDestroyedTimerMax = 5f;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        IsDestroyed = false;
    }

    private void Start()
    {
        beaterPlayer.OnBeaterThrow += beaterPlayer_OnBeaterThrow;
        BeaterPlayer.OnBeaterDestroy += BeaterPlayer_OnBeaterDestroy;
    }

    private void BeaterPlayer_OnBeaterDestroy(object sender, BeaterPlayer.OnBeaterDestroyArgs e)
    {
        if (e.bludgerBallToDistroy == this)
        {
            DestroyBall();
        }
    }


    private void Update()
    {

        if (IsDestroyed)
        {
            respawnAfterDestroyedTimer -= Time.deltaTime;
            if (respawnAfterDestroyedTimer < 0)
            {
                IsDestroyed = false;
                OnBludgerDestroyStatusChange?.Invoke(this, EventArgs.Empty);
            }
        }
    }

    private void beaterPlayer_OnBeaterThrow(object sender, BeaterPlayer.OnBeaterThrowArgs e)
    {
        transform.parent = null;
        Vector3 direction = e.position - transform.position;
        rigidbody.velocity = direction.normalized * throwPower;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other is CapsuleCollider && other.TryGetComponent<ChaserPlayer>(out ChaserPlayer chaserPlayer))
        {
            foreach (Transform enemyChaser in enemyChaseres)
            {
                if (enemyChaser == chaserPlayer.transform)
                {
                    OnBludgerHit?.Invoke(this, new OnBludgerHitArgs
                    {
                        hittedPlayerCollider = other
                    });
                    returnBludgetToPlayer();
                }
            }


        }
        if (other.TryGetComponent<PlayGroundCollider>(out PlayGroundCollider collider))
        {
            if (transform.parent == null)
            {
                DestroyBall();

            }
        }
    }

    private void DestroyBall()
    {
        returnBludgetToPlayer();
        respawnAfterDestroyedTimer = respawnAfterDestroyedTimerMax;
        IsDestroyed = true;
        OnBludgerDestroyStatusChange?.Invoke(this, EventArgs.Empty);

        // GetComponent<MeshRenderer>().material = destroyedMaterial;
    }

    private void returnBludgetToPlayer()
    {
        transform.position = beaterPlayer.ballHoldingPosition.position;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularDrag = 0;
        rigidbody.angularVelocity = Vector3.zero;
        transform.parent = beaterPlayer.ballHoldingPosition;
        transform.localPosition = Vector3.zero;
        beaterPlayer.IsHoldingBludgerBall = true;
    }
}
